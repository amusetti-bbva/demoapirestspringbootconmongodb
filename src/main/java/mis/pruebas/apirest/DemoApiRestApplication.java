package mis.pruebas.apirest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.filter.ForwardedHeaderFilter;

@SpringBootApplication
public class DemoApiRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApiRestApplication.class, args);
	}

	// Solicitud HTTP -> [filtro1 -> filtro2 ->filtro3 -> ... -> fwdHdrFltr ->.... -> filtroN] -> respuesta
	@Bean
	public ForwardedHeaderFilter forwardedHeaderFilter() {
		// X-Forwarded-Host
		// X-Forwarded-Port
		// X-Forwarded-Proto
		return new ForwardedHeaderFilter();
	}
}
