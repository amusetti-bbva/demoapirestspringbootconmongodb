package mis.pruebas.apirest.servicios.repositorios;

import mis.pruebas.apirest.modelos.Cuenta;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositorioCuenta extends MongoRepository<Cuenta,String> {
}
